# -*- coding: UTF-8 -*-
import StringIO
import os

from PIL import Image, ImageDraw, ImageFont
from io import BytesIO

ROOT_DIR = os.path.dirname(__file__)

COLOR_WHITE = '#FFFFFF'
IMAGE_SIZE = 150
TEXT_SIZE = 66
FONT_FILE_PATH = 'font/兰亭黑GBK.TTF'

TYPE_GROUP = 'group'
TYPE_LECTURE = 'lecture'
TYPE_USER = 'user'
BACKGROUND_IMAGE = {TYPE_USER: 'file/bg_member.png',
                    TYPE_LECTURE: 'file/bg_lecture.png',
                    TYPE_GROUP: 'file/bg_group.png'}

SMALL_IMAGE_SIZE = (42, 42)
BIG_IMAGE_SIZE = (63, 63)
MAX_TEXT_LIMIT = 5
FILE_PATH_BG_IMAGE = 'file/bg_avatar.png'

IMAGE_BOX_1 = [(45, 45, 108, 108)]
IMAGE_BOX_2 = [(9, 45, 72, 108), (78, 45, 141, 108)]
IMAGE_BOX_3 = [(42, 9, 105, 72), (9, 78, 72, 141), (78, 78, 141, 141)]
IMAGE_BOX_4 = [(9, 9, 72, 72), (78, 9, 141, 72), (9, 78, 72, 141),
               (78, 78, 141, 141)]
IMAGE_BOX_5 = [(30, 30, 72, 72), (75, 30, 117, 72), (9, 75, 51, 117),
               (54, 75, 96, 117),
               (99, 75, 141, 117)]
IMAGE_BOX_6 = [(9, 30, 51, 72), (54, 30, 96, 72),
               (99, 30, 141, 72), (9, 75, 51, 117), (54, 75, 96, 117),
               (99, 75, 141, 117)]
IMAGE_BOX_7 = [(54, 9, 96, 51), (9, 54, 51, 96), (54, 54, 96, 96),
               (99, 54, 141, 96), (9, 99, 51, 141), (54, 99, 96, 141),
               (99, 99, 141, 141)]

IMAGE_BOX_8 = [(30, 9, 72, 51), (75, 9, 117, 51), (9, 54, 51, 96),
               (54, 54, 96, 96),
               (99, 54, 141, 96), (9, 99, 51, 141), (54, 99, 96, 141),
               (99, 99, 141, 141)]
IMAGE_BOX_9 = [(9, 9, 51, 51), (54, 9, 96, 51),
               (99, 9, 141, 51), (9, 54, 51, 96), (54, 54, 96, 96),
               (99, 54, 141, 96), (9, 99, 51, 141), (54, 99, 96, 141),
               (99, 99, 141, 141)]

IMAGE_MAP = {1: IMAGE_BOX_1, 2: IMAGE_BOX_2, 3: IMAGE_BOX_3, 4: IMAGE_BOX_4,
             5: IMAGE_BOX_5,
             6: IMAGE_BOX_6, 7: IMAGE_BOX_7, 8: IMAGE_BOX_8, 9: IMAGE_BOX_9}


def create_image(content_list):
    if len(content_list) < MAX_TEXT_LIMIT:
        size = BIG_IMAGE_SIZE
    else:
        size = SMALL_IMAGE_SIZE
    bg_image = Image.open(os.path.join(ROOT_DIR, FILE_PATH_BG_IMAGE))
    n = 0
    while n < len(content_list):
        img = Image.open(StringIO.StringIO(content_list[n]))
        # 图片宽高不相同时,thumbnail后宽高不相同,会报错,先resize一下
        w, h = img.size
        if w > h:
            c_size = h
        else:
            c_size = w
        image_out = img.resize((c_size, c_size))
        image_out.thumbnail(size)
        bg_image.paste(image_out, IMAGE_MAP[len(content_list)][n])
        n += 1
    output = BytesIO()
    bg_image.save(output, 'PNG')
    return output.getvalue()


def create_text_image(text, type):
    bg_image_path = os.path.join(ROOT_DIR, BACKGROUND_IMAGE.get(type))
    image_bg = Image.open(bg_image_path)
    draw = ImageDraw.Draw(image_bg)
    font = ImageFont.truetype(os.path.join(ROOT_DIR, FONT_FILE_PATH), TEXT_SIZE)
    w, h = draw.textsize(text, font=font)
    offw, offh = font.getoffset(text)  # 去掉称线
    width = (IMAGE_SIZE - w - offw) / 2
    height = (IMAGE_SIZE - h - offh) / 2
    draw.text((width, height), text, font=font, fill=COLOR_WHITE)
    output = BytesIO()
    image_bg.save(output, 'PNG')
    return output.getvalue()
