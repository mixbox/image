# coding=utf8
import requests


def download_image(image_list):
    return [requests.get(url).content for url in image_list]
