import os

debug = bool(int(os.getenv('DEBUG', '1')))
access_key = os.getenv('ACCESS_KEY', '')
secret_key = os.getenv('SECRET_KEY', '')
bucket_name = os.getenv('BUCKET_NAME', '')
base_http_url = os.getenv('BASE_HTTP_URL', 'http://%s/%s')
download_host = os.getenv('DOWNLOAD_HOST', '')
