# -*- coding: UTF-8 -*-
import logging

import datetime
import config
import flask
import requests

import download_manager
import qiniu_manager
import image_process_manager
from flasgger import Swagger

from flask import Flask, redirect
from flask import request
from functools import wraps

from flask.ext.api import status
from werkzeug.contrib.jsrouting import render_template
from werkzeug.contrib.profiler import ProfilerMiddleware
from werkzeug.wrappers import BaseResponse

handler = logging.StreamHandler()
handler.setLevel(logging.WARN)
FORMAT = '%(asctime)-15s %(levelname)s  %(message)s'
logging.basicConfig(format=FORMAT)

app = Flask(__name__)
app.logger.addHandler(handler)

if config.debug:  # DEBUG
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app,
                                      profile_dir='/tmp/image_prof/')

STATUS_SUCCESS = 0;
STATUS_FAILED = -1;

Swagger(app)


def render_html(template, **defaults):
    def wrapped(result):
        variables = defaults.copy()
        variables.update(result)
        return render_template(template, **variables)

    return wrapped


def render_json(**defaults):
    def wrapped(result):
        variables = defaults.copy()
        variables.update(result)
        return flask.jsonify(variables)

    return wrapped


def view(url, renderer=None, *args, **kwargs):
    super_route = app.route

    defaults = kwargs.pop('defaults', {})
    route_id = object()
    defaults['_route_id'] = route_id

    def deco(f):
        @super_route(url, defaults=defaults, *args, **kwargs)
        @wraps(f)
        def decorated_function(*args, **kwargs):
            this_route = kwargs.get('_route_id')
            if not getattr(f, 'is_route', False):
                del kwargs['_route_id']

            result = f(*args, **kwargs)

            if this_route is not route_id:
                return result

            # catch redirects.
            if isinstance(result, (app.response_class,
                                   BaseResponse)):
                return result

            if renderer is None:
                return result
            return renderer(result)

        decorated_function.is_route = True
        return decorated_function

    return deco


@app.route('/')
def hello_world():
    return redirect('/apidocs/index.html')


@view('/integrate/image/compose', renderer=render_json(), methods=['POST'])
def integrate_image():
    '''
    整合图片,将1-9张图片按照特定排序生成一张图片并上传到七牛
    ---
    tags:
      - image
    parameters:
      - in: body
        name: body
        schema:
          id: image
          required:
            - image_urls
            - filename
          properties:
            image_urls:
              type: array
              items:
                type: string
              description: 图片链接
            filename:
              type: string
              description: 图片名称
    '''
    image_urls = request.json.get('image_urls')
    file_name = request.json.get('filename')
    if not image_urls:
        return dict(code=STATUS_FAILED, msg='image_url 字段不能为空')
    elif not file_name:
        return dict(code=STATUS_FAILED, msg='filename 字段不能为空')
    logging.warn('params = ' + str(request.json))
    start_time = datetime.datetime.now()
    file_image_path = image_process_manager.create_image(
        download_manager.download_image(image_urls))
    qiniu_manager.delete_file(file_name)
    upload_response = qiniu_manager.upload_file(file_image_path, file_name)
    end_time = datetime.datetime.now()
    logging.warn('time = %s' % (str(end_time - start_time)))
    if not upload_response.ok:
        return dict(code=STATUS_FAILED, msg='图片上传七牛服务器失败,请稍后重试')
    return dict(code=STATUS_SUCCESS,
                data=dict(url=qiniu_manager.get_download_url(file_name)))


@view('/integrate/image/text', renderer=render_json(), methods=['POST'])
def integrate_text_image():
    '''
    整合图片,将1-9张图片按照特定排序生成一张图片并上传到七牛
    ---
    tags:
      - image
    parameters:
      - in: body
        name: body
        schema:
          id: image
          required:
            - type
            - name
            - filename
          properties:
            type:
              type: enum
              enum:
                - user
                - group
                - lucture
              description: 背景类型
            name:
              type: string
              description: 文字
            filename:
              type: string
              description: 文件名
    '''
    request_data = request.json
    type = request_data.get('type')
    text = request_data.get('name')
    file_name = request_data.get('filename')
    if text is not None and len(text) > 1:
        text = text[0]
    if not type:
        return dict(code=STATUS_FAILED, msg='type 字段不能为空')
    elif not text:
        return dict(code=STATUS_FAILED, msg='name 字段不能为空')
    elif not file_name:
        return dict(code=STATUS_FAILED, msg='filename 字段不能为空')
    else:
        download_url = qiniu_manager.get_download_url(file_name)
        if not requests.get(download_url).status_code == status.HTTP_200_OK:
            file_image_path = image_process_manager.create_text_image(text,
                                                                      type)
            upload_response = qiniu_manager.upload_file(file_image_path,
                                                        file_name)
            if not upload_response.ok:
                return dict(code=STATUS_FAILED, msg='图片上传七牛服务器失败,请稍后重试')
        return dict(code=STATUS_SUCCESS, data=dict(url=download_url))


if __name__ == '__main__':
    app.debug = True
    app.run(host='127.0.0.1', port=8002)
