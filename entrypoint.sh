#!/usr/bin/env bash

sed -i "s/--workers [[:digit:]]*/--workers ${WORKERS:-10} /" /etc/supervisor/conf.d/app.conf

# 青云的 DNS 解析七牛 CDN 域名 7xid0k.com1.z0.glb.clouddn.com 会卡到超时，换掉
# resolv.conf 被 docker 设置为不能删除 https://github.com/docker/docker/issues/9295
sed '/nameserver/i nameserver 202.106.46.151' /etc/resolv.conf > /tmp/resolv
cat /tmp/resolv > /etc/resolv.conf

supervisord --nodaemon -c /etc/supervisor/supervisord.conf
