# coding:utf-8
import json
import unittest
from io import BytesIO

import config
import image_controller
import image_process_manager
import mock
import qiniu_manager
from PIL import Image
from flask.ext.api import status


class MockResponse:
    '''自定义响应对象，mock请求时统一返回此对象
    '''

    def __init__(self, json_data, status_code, ok=True):
        self.json_data = json_data
        self.status_code = status_code
        self.ok = ok

    def json(self):
        return self.json_data


class ImageProcessTest(unittest.TestCase):
    def _create_bg_image(self, filename='file/bg_avatar.png'):
        output = BytesIO()
        test_image = Image.open(filename)
        test_image.save(output, 'PNG')
        return output

    def _create_test_image(self, count=1):
        i = 0
        output = []
        while i < count:
            with open('file/test/test.jpg', 'rb') as test_file:
                output.append(test_file.read())
            i += 1
        return output

    def setUp(self):
        super(ImageProcessTest, self).setUp()

    def test_create_image(self):
        '''测试图片整合方法,从1张图片测试到9张图片'''
        i = 1
        while i <= 9:
            bg_output = self._create_bg_image()
            image_list = self._create_test_image(i)
            output = image_process_manager.create_image(image_list)
            self.assertIsNotNone(output)
            self.assertNotEqual(output, bg_output)
            i += 1

    def test_create_text_image(self):
        '''测试图片添加文字方法'''
        bg_user = self._create_bg_image('file/bg_member.png')
        output_user = image_process_manager.create_text_image('U',
                                                              image_process_manager.TYPE_USER)
        self.assertIsNotNone(output_user)
        self.assertNotEqual(bg_user, output_user)

        bg_group = self._create_bg_image('file/bg_group.png')
        output_group = image_process_manager.create_text_image('G',
                                                               image_process_manager.TYPE_GROUP)
        self.assertIsNotNone(output_group)
        self.assertNotEqual(bg_group, output_group)

        bg_lecture = self._create_bg_image('file/bg_lecture.png')
        output_lecture = image_process_manager.create_text_image('L',
                                                                 image_process_manager.TYPE_LECTURE)
        self.assertIsNotNone(output_lecture)
        self.assertNotEqual(bg_lecture, output_lecture)


class ImageTest(unittest.TestCase):
    def setUp(self):
        self.app = image_controller.app.test_client()
        config.debug = False
        self.image_compose_data = dict(
            image_urls=['image_1', 'image_2', 'image_3'],
            filename='filename')
        self.image_text_data = dict(type='user',
                                    name='name',
                                    filename='filename')

    def test_image_compose_with_none_image_urls(self):
        '''测试拼接图片接口参数没有image_urls'''
        data = self.image_compose_data
        data.pop('image_urls')
        response = self.app.post('/integrate/image/compose',
                                 data=json.dumps(data),
                                 content_type='application/json')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_FAILED)

    def test_image_compose_with_none_filename(self):
        '''测试拼接图片接口参数没有filename'''
        data = self.image_compose_data
        data.pop('filename')
        response = self.app.post('/integrate/image/compose',
                                 data=json.dumps(data),
                                 content_type='application/json')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_FAILED)

    def test_image_compose_with_upload_failed(self):
        '''测试拼接图片接口上传失败'''
        data = self.image_compose_data
        with mock.patch('image_process_manager.create_image', return_value=''):
            with mock.patch('download_manager.download_image', return_value=''):
                with mock.patch('qiniu_manager.delete_file', return_value=''):
                    with mock.patch('qiniu_manager.upload_file',
                                    return_value=MockResponse(
                                        status_code=status.HTTP_200_OK,
                                        json_data='', ok=False)):
                        response = self.app.post('/integrate/image/compose',
                                                 data=json.dumps(data),
                                                 content_type='application/json')

        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_FAILED)

    def test_image_compose(self):
        '''测试拼接图片接口'''
        data = self.image_compose_data
        with mock.patch('image_process_manager.create_image', return_value=''):
            with mock.patch('download_manager.download_image', return_value=''):
                with mock.patch('qiniu_manager.delete_file', return_value=''):
                    with mock.patch('qiniu_manager.upload_file',
                                    return_value=MockResponse(
                                        status_code=status.HTTP_200_OK,
                                        json_data='')):
                        response = self.app.post('/integrate/image/compose',
                                                 data=json.dumps(data),
                                                 content_type='application/json')

        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_SUCCESS)
        self.assertEqual(response_json['data']['url'],
                         qiniu_manager.get_download_url(data['filename']))

    def test_image_text_with_none_type(self):
        '''测试图片添加文字接口参数不带type'''
        data = self.image_text_data
        data.pop('type')
        response = self.app.post('/integrate/image/text',
                                 data=json.dumps(data),
                                 content_type='application/json')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_FAILED)

    def test_image_text_with_none_name(self):
        '''测试图片添加文字接口参数不带name'''
        data = self.image_text_data
        data.pop('name')
        response = self.app.post('/integrate/image/text',
                                 data=json.dumps(data),
                                 content_type='application/json')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_FAILED)

    def test_image_text_with_none_filename(self):
        '''测试图片添加文字接口参数不带filename'''
        data = self.image_text_data
        data.pop('filename')
        response = self.app.post('/integrate/image/text',
                                 data=json.dumps(data),
                                 content_type='application/json')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_FAILED)

    def test_image_text_with_upload_failed(self):
        '''测试图片添加文字接口上传失败'''
        data = self.image_text_data
        with mock.patch('requests.get', return_value=MockResponse(
                status_code=status.HTTP_404_NOT_FOUND, json_data='')):
            with mock.patch('image_process_manager.create_text_image',
                            return_value=''):
                with mock.patch('qiniu_manager.upload_file',
                                return_value=MockResponse(
                                    status_code=status.HTTP_200_OK,
                                    json_data='', ok=False)):
                    response = self.app.post('/integrate/image/text',
                                             data=json.dumps(data),
                                             content_type='application/json')

        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_FAILED)

    def test_image_text_with_file_exist(self):
        '''测试图片添加文字接口图片已存在'''
        data = self.image_text_data
        with mock.patch('requests.get', return_value=MockResponse(
                status_code=status.HTTP_200_OK, json_data='')):
            response = self.app.post('/integrate/image/text',
                                     data=json.dumps(data),
                                     content_type='application/json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_json = json.loads(response.data)
        self.assertEqual(response_json['code'], image_controller.STATUS_SUCCESS)
        self.assertEqual(response_json['data']['url'],
                         qiniu_manager.get_download_url(data['filename']))

    def test_image_text(self):
        '''测试图片添加文字接口'''
        data = self.image_text_data
        with mock.patch('requests.get', return_value=MockResponse(
                status_code=status.HTTP_404_NOT_FOUND, json_data='')):
            with mock.patch('image_process_manager.create_text_image',
                            return_value=''):
                with mock.patch('qiniu_manager.upload_file',
                                return_value=MockResponse(
                                    status_code=status.HTTP_200_OK,
                                    json_data='')):
                    response = self.app.post('/integrate/image/text',
                                             data=json.dumps(data),
                                             content_type='application/json')

        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json['code'], image_controller.STATUS_SUCCESS)
        self.assertEqual(response_json['data']['url'],
                         qiniu_manager.get_download_url(data['filename']))


if __name__ == '__main__':
    unittest.main()
