FROM localhost:5000/etu-base
MAINTAINER Xie Yanbo <xieyanbo@gmail.com>

ENV DEBUG='0'
ENV ACCESS_KEY=''
ENV SECRET_KEY=''
ENV BUCKET_NAME=''
ENV BASE_HTTP_URL='http://%s/%s'
ENV DOWNLOAD_HOST=''


ADD ./requirements.txt /app/
RUN pip install --trusted-host mirrors.aliyun.com -i http://mirrors.aliyun.com/pypi/simple/ -r /app/requirements.txt \
    && pip install --trusted-host mirrors.aliyun.com -i http://mirrors.aliyun.com/pypi/simple/ gunicorn requests mysql-python
ADD ./supervisor.conf /etc/supervisor/conf.d/app.conf
RUN cp /etc/nginx/sites-available/nginx-app-default.conf /etc/nginx/sites-enabled/default
WORKDIR /app
ADD ./entrypoint.sh /entrypoint.sh
ENTRYPOINT ["bash", "/entrypoint.sh"]

ADD . /app
