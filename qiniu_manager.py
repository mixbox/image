# coding=utf8
# flake8: noqa
from qiniu import Auth, put_data, BucketManager

import config


def upload_file(data, upload_file_name):
    # 构建鉴权对象
    q = Auth(config.access_key, config.secret_key)
    # 生成上传 Token，可以指定过期时间等
    token = q.upload_token(config.bucket_name, upload_file_name, 3600)
    ret, info = put_data(token, upload_file_name, data)
    return info


def delete_file(upload_file_name):
    # print 'upload_file_name = ', upload_file_name
    q = Auth(config.access_key, config.secret_key)
    bucket = BucketManager(q)
    bucket.delete(config.bucket_name, upload_file_name)


def get_download_url(file_name):
    return config.base_http_url % (config.download_host, file_name)
